package com.example.atomicaxis.androidbootcamp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Atomic on 11/3/2014.
 */
public class CustomDrawerAdapter extends ArrayAdapter<DrawerItem> {

    Context context;
    List<DrawerItem> drawerItemList;
    int layoutResID;
    TextView ItemName;
    ImageView Icon;

    public CustomDrawerAdapter(Context context, int layoutResID, List<DrawerItem> listItems){

        super(context,layoutResID,listItems);
        this.context = context;
        this.layoutResID = layoutResID;
        drawerItemList =listItems;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = null;

        if (row == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
             row= inflater.inflate(layoutResID, parent,false);

        } else {
            row = convertView;
        }

        ItemName = (TextView) row.findViewById(R.id.drawer_itemName);
        Icon = (ImageView) row.findViewById(R.id.drawer_icon);
        ItemName.setText(drawerItemList.get(position).getItemName());
        Icon.setImageResource(drawerItemList.get(position).getImgResID());
        return row;

    }
}
