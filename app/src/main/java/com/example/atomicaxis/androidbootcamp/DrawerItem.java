package com.example.atomicaxis.androidbootcamp;

/**
 * Created by Atomic on 11/3/2014.
 */
public class DrawerItem {

    String ItemName;
    int imgResID;

    public DrawerItem(String itemName, int imgResID) {
        ItemName=itemName;
        this.imgResID=imgResID;
    }

    public String getItemName()
    {
        return ItemName;
    }

    public int getImgResID(){
        return imgResID;
    }
}
