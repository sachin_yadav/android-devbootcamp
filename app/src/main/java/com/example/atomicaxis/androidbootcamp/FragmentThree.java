package com.example.atomicaxis.androidbootcamp;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Atomic on 11/3/2014.
 */
public class FragmentThree extends Fragment {
    ImageView Icon;
    TextView ItemName;
    public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    public FragmentThree(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_layout_three,container, false);

        Icon=(ImageView)view.findViewById(R.id.frag3_icon);
        ItemName=(TextView)view.findViewById(R.id.frag3_text);

        ItemName.setText(getArguments().getString(ITEM_NAME));
        Icon.setImageDrawable(view.getResources().getDrawable(
                getArguments().getInt(IMAGE_RESOURCE_ID)));
        return view;
    }
}
