package com.example.atomicaxis.androidbootcamp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LoginPage extends Activity {
    EditText UserName;
    EditText Password;
    Button LogIn;
    Button SignUp;
    String userNameText;
    String passwordText;
    ParseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        //Initialize the parse connection
        Parse.initialize(this, "oZ38UuVbGjsg6orc2wIyhViAb22Q6gwV8g2RfCZI", "El12RMAlFkGZcI0rCQuSQ1DdLZxmRtANjEIazTM1");
        user = new ParseUser();

        //Initializes the EditText and Buttons from our layout XML file.
        UserName = (EditText) findViewById(R.id.Username);
        Password = (EditText) findViewById(R.id.Password);
        LogIn = (Button) findViewById(R.id.LogIn);
        SignUp = (Button) findViewById(R.id.SignUp);
        logInButtonIsClicked();
        signUpButtonIsClicked();
    }

    public void logInButtonIsClicked() {


        //What happens when Log in button is clicked
        LogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Saves the login info from the user
                userNameText = UserName.getText().toString();
                passwordText = Password.getText().toString();

                //Verifies the login info with Parse backend
                user.logInInBackground(userNameText, passwordText, new LogInCallback() {
                    public void done(ParseUser users, ParseException e) {
                        if (users != null) {
                            //Sends the user to the home page if the login info is correct
                            Intent goToHomePage = new Intent(LoginPage.this, HomePage.class);
                            onPause();
                            startActivity(goToHomePage);

                        } else {
                            //Creates a toast if the login info is incorrect
                            Context context = getApplicationContext();
                            CharSequence text = "Invalid Login";
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            UserName.getText().clear();
                            Password.getText().clear();

                        }
                    }
                });

            }
        });
    }


    public void signUpButtonIsClicked() {

        //What happens when Sign Up button is clicked
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToSignUpPage = new Intent(view.getContext(), SignUpPage.class);
                startActivity(goToSignUpPage);

            }
        });
    }

}

