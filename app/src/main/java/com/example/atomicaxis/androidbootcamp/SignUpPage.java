package com.example.atomicaxis.androidbootcamp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;


public class SignUpPage extends Activity {
    EditText UserName;
    EditText Password;
    Button SignUp;
    String userNameText;
    String passwordText;
    ParseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_page);
        user = new ParseUser();

        // Initializes the EditText and Buttons from our layout XML file.
        UserName = (EditText) findViewById(R.id.SignUp_Username);
        Password = (EditText) findViewById(R.id.SignUp_Password);
        SignUp = (Button) findViewById(R.id.SignUp_signup);

        signUpButtonIsClicked();
    }


    public void signUpButtonIsClicked() {
        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameText = UserName.getText().toString();
                passwordText = Password.getText().toString();
                user.setUsername(userNameText);
                user.setPassword(passwordText);

                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            Intent goToLoginPage = new Intent(SignUpPage.this, LoginPage.class);
                            startActivity(goToLoginPage);
                        } else {
                            Context context = getApplicationContext();
                            CharSequence text = "Unable to Sign Up";
                            int duration = Toast.LENGTH_SHORT;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            UserName.getText().clear();
                            Password.getText().clear();
                        }
                    }
                });
            }
        });
    }
}

